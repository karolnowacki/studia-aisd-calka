#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <float.h>

/* Definicja typu - wskaźnik na funkcję - później funkcja podcałkowa
	i funkcja rozwiązania analitycznego muszą być tego typ */
typedef double func_t(double);

/* Definicja stuktuty przypadku testowego */
typedef struct testcase testcase;
struct testcase {
	char * name;  // nazwa funkcji (używana w komunikatach)
	func_t * func;  // wskaźnik na funkcję pod całką
	func_t * integral_function; // wskaźnik na funkcję rozwiązania analitycznego
	double range_from; // domyślna dolna granica całkowania
	double range_to;  // domyślna górna granica całkowania
  double fmin;  // domyślna wartość minimalna dla funkcji na przedziele całkowania dla metody MC
	double fmax;  // domyślna wartość maksymalna dla funkcji na przedziale całkowania dld metody MC
};

/* funkcja implementująca metodę prostokątów. Przyjmuje wskaźnik na funkcję podcałkowa, granice całkowania (from, to) oraz liczbę iteracji */
double integral_rectangle(func_t * func, double range_from, double range_to, int iterations) {

	double step = (range_to - range_from)/iterations; // wielkośc kroku
	double x = range_from+step*0.5; // aktualna wartość x w danym kroku iteracji
	double sum = 0.0; // wartosc calki
	double err = 0.0; // warość błędu obciecia bitów dla sumy Kahana
	double grad = 0.0; // wielkość przyrostu sumy w danym kroku
	double tmp;

	while (x < range_to) { // dopóki nie dojdziemy do końca przedziału całkowania
    tmp = func(x); // wartość funkcji w danym kroku
		if (isnormal(tmp)) {   // sprawdzamy czy funkcja jest określona w tym punkcie
			grad = tmp - err; // przyrost sumy minus poprawka błedu zaokrąglenia
			tmp = sum + grad; // zapisujemy suma + przyrost do zmiennej tymczasowej...
			err = (tmp - sum) - grad; /// żeby policzyć błąd obcięcia bitów
			sum = tmp;
    }
		x += step; // przesuwamy się krok dalej
	}
	sum *= step;

	return sum;
}

double integral_trapez(func_t * func, double range_from, double range_to, int iterations) {

  /* zmienne podobnie jak w metodzie prostokątów */
	double step = (range_to - range_from)/iterations;
	double x = range_from;
	double sum = 0.0;
	double err = 0.0;
	double grad = 0.0;
	double tmp;

  double a,b; // a - wartosc funkcji w x, b - wartosc w punkcie x+step

	range_to -= step*0.5;

	a = func(x);
	while (!isnormal(a)) { a = func(x+=step); } // upewniamy się, że funkcja jest określona w tym punkcie

	while (x < range_to) {
		b = func(x+= step);
		if (isnormal(b)) { // sprawdzamy, czy funkcja jest określona w tych punktach
			grad = a+b - err; // sumowanie jak w metodzie prostokątów
			tmp = sum + grad;
			err = (tmp - sum) - grad;
			sum = tmp;
			a = b;
		}
	}
	sum *= step*0.5;

	return sum;

}

double integral_monte_carlo(func_t * func, double range_from, double range_to, int iterations, double fmin, double fmax) {
	int hit = 0;
	int i;

	double x,y,fy;

	for (i = 0; i<iterations; ++i) {
		x = drand48()*(range_to-range_from)+range_from; //losujemy wartość x
		y = drand48()*(fmax-fmin)+fmin; // losujemy wartość y

		fy = func(x); // wartość funkcji w punkcjie y
		if (!isnormal(fy)) // sprawdzamy czy funkcja jest określona w punkcie x
			continue;

		if (y > 0 && fy > y) // jeżeli y jest pod wykresem funkcji i funkcja jest dodatnia w punkcie x
			hit++; // zwiększamy trafienia
		if (y < 0 && fy < y) // jeżelu y jest pod wykresem funkcji i funkcja jest ujemna w punkcjie x
			hit--; // zmniejszamy trafienia
	}
	return (fmax-fmin)*(range_to-range_from) * hit / iterations;
}

/* dla liczenia czasu wykonywania się funkcji, poniższa funkcja zwraca różnicę
	czasu między dwoma strukturami timeval w mikrosekundach */
long long int diffTimeMicroSec(struct timeval s, struct timeval e) {
	return ((e.tv_sec - s.tv_sec)*1000000L + e.tv_usec) - s.tv_usec;
}

/* PRZYPADKI TESTOWE */
double fun1(double x) {	return sin(x); }
double int_fun1(double x) { return -cos(x); }

double fun2(double x) { return x*x + 2*x + 3; }
double int_fun2(double x) {	return 1.0/3.0*x*x*x + x*x + 3*x; }

double fun3(double x) { return cos(x)/sin(x); }
double int_fun3(double x) { return log(sin(x)); }

double fun4(double x) { return x*(x+cos(x)); }
double int_fun4(double x) { return x*x*x/3.0+x*sin(x)+cos(x); }

double fun5(double x) { return exp(x); }

/* Definicja przypadków testowych wraz z domyślnymi granicami całkowania */

/* Struktura: {
	nazwa,
	wskaznik na fukcje podcałkowa,
	wskażnik na funkje rozwiazania analitycznego
	dolna granica całkowania
	górna granica całkowania
	okraniczenie dolne
	ograniczenie górne
} */
testcase tc[] = {
	{ "sin(x)", fun1, int_fun1, 0, M_PI, -1, 1 },
	{ "x^2+2x+3", fun2, int_fun2, -4, 4, 0, 30 },
	{ "ctg(x)", fun3, int_fun3, 0.1, 3.0, -10, 10},
	{ "x*(x+cos(x))", fun4, int_fun4, 0, M_PI, 0, 10 },
	{ "e^x", fun5, fun5, 0, 1, 0, 5}
};

//Liczba interacji
#define ITERATIONS 1000

int main(int argc, char *argv[]) {
	int i,ischanged; // licznik petli, flaga czy została zmienie domyślne granice całkowania
	testcase *t; // wskażnik na aktualny przpadek testowy
	char * ui = NULL; // wskażnik na bufor do getfile
	size_t n = 0; // długość powyższego bufora
	double r,ar,tmp; // r - rozwiązanie, ar - rozwiązanie analityczne
	int tests = sizeof tc/sizeof *tc; // Liczba testów
	struct timeval startTime, endTime; // czas przd i po uruchomieniu funkcji


	for (i=0; i<tests; ++i) { // dla wszystkich przypadków testowych :
		t = tc+i; //aktualny przypadek testowy

		printf("Test function %d f(x)=%s:\n", i+1, t->name);
		printf("\t\t- podaj dolna granice calkowania [domyslnie: %f]: ", t->range_from);

		ischanged = 0;

		getline(&ui, &n, stdin);
		if (sscanf(ui, "%lf", &tmp) == 1) { t->range_from = tmp; ischanged = 1; }
		free(ui); ui = NULL; n = 0;

		printf("\t\t- podaj gorna granice calkowania [domyslnie: %f]: ", t->range_to);
		getline(&ui, &n, stdin);
		if (sscanf(ui, "%lf", &tmp) == 1) { t->range_to = tmp; ischanged = 1; }
		free(ui); ui = NULL; n = 0;

		if (ischanged) {
			do {
				printf("\t\t- podaj ograniczenie gorne funcji w przedziale [%f, %f]: ", t->range_from, t->range_to);
			} while (scanf("%lf", &(t->fmax)) != 1);
			do {
				printf("\t\t- podaj ograniczenie dolne funcji w przedziale [%f, %f]: ", t->range_from, t->range_to);
			} while (scanf("%lf", &(t->fmin)) != 1);
		}

		if (t->fmin > t->fmax) { tmp = t->fmin; t->fmin = t->fmax; t->fmax = tmp; }
		printf("\tCalkowana funkcja na przedziale [%f,%f] jest ograniczona [%f, %f]\n\n", t->range_from, t->range_to, t->fmin, t->fmax);

		gettimeofday (&startTime, NULL);
		ar = t->integral_function(t->range_to) - t->integral_function(t->range_from);
		gettimeofday (&endTime, NULL);
		printf("\tRozwiązanie analityczne:\t%f (czas: %Ld us)\n", ar, diffTimeMicroSec(startTime, endTime));

		gettimeofday (&startTime, NULL);
		r = integral_rectangle(t->func, t->range_from, t->range_to, ITERATIONS);
		gettimeofday (&endTime, NULL);
		printf("\tMetoda prostokatow:\t\t%f (blad: %8e, czas: %Ld us)\n", r, fabs(r-ar), diffTimeMicroSec(startTime, endTime));

		gettimeofday (&startTime, NULL);
		r = integral_trapez(t->func, t->range_from, t->range_to, ITERATIONS);
		gettimeofday (&endTime, NULL);
		printf("\tMetoda trapezow:\t\t%f (blad: %8e, czas: %Ld us)\n", r, fabs(r-ar), diffTimeMicroSec(startTime, endTime));

		gettimeofday (&startTime, NULL);
		r = integral_monte_carlo(t->func, t->range_from, t->range_to, ITERATIONS, t->fmin, t->fmax);
		gettimeofday (&endTime, NULL);
		printf("\tMetoda monte carlo:\t\t%f (blad: %8e, czas: %Ld us)\n", r, fabs(r-ar), diffTimeMicroSec(startTime, endTime));

		printf("\n");
	}

	return 0;
}
